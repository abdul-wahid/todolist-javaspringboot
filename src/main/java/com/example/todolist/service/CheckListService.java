package com.example.todolist.service;

import com.example.todolist.entity.CheckList;
import com.example.todolist.entity.Item;

import java.util.List;

public interface CheckListService {
    CheckList create(CheckList checkList);
    List<CheckList> getAll();
    void deleteById(String id);
}
