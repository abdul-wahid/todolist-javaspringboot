package com.example.todolist.service;

import com.example.todolist.entity.Item;

import java.util.List;

public interface ItemService {
    List<Item> getAll();
    Item create(Item item);
    Item update(Item item);
    Item getById(String id);
    Item updateStatus(String id, Boolean status);
    void deleteById(String id);
}
