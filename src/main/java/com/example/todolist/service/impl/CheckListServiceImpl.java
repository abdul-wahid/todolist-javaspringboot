package com.example.todolist.service.impl;

import com.example.todolist.entity.CheckList;
import com.example.todolist.entity.Item;
import com.example.todolist.repository.CheckListRepository;
import com.example.todolist.service.CheckListService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CheckListServiceImpl implements CheckListService {
    private final CheckListRepository checkListRepository;

    @Override
    public CheckList create(CheckList checkList) {
        try {
            return checkListRepository.saveAndFlush(checkList);
        } catch (DataIntegrityViolationException exception) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "checklist already used");
        }
    }

    @Override
    public List<CheckList> getAll() {
        return checkListRepository.findAll();
    }

    public CheckList getById(String id){
        return checkListRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "checklist not found"));
    }

    @Override
    public void deleteById(String id) {
        CheckList checkList = getById(id);
        checkListRepository.delete(checkList);
    }
}
