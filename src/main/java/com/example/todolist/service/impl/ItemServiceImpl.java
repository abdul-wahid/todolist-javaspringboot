package com.example.todolist.service.impl;

import com.example.todolist.entity.Item;
import com.example.todolist.repository.ItemRepository;
import com.example.todolist.service.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {
    private final ItemRepository itemRepository;

    @Override
    public List<Item> getAll() {
        return itemRepository.findAll();
    }

    @Override
    public Item create(Item item) {
        try {
            return itemRepository.saveAndFlush(item);
        } catch (DataIntegrityViolationException exception) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "item already used");
        }
    }

    @Override
    public Item update(Item item) {
        Item currentItem = getById(item.getId());
        return itemRepository.save(currentItem);
    }

    @Override
    public Item getById(String id) {
        return itemRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "item not found"));
    }

    @Override
    public Item updateStatus(String id, Boolean status) {
        Item currentItem = getById(id);
        currentItem.setStatus(status);
        return itemRepository.save(currentItem);
    }

    @Override
    public void deleteById(String id) {
        Item currentItem = getById(id);
        itemRepository.delete(currentItem);
    }
}
