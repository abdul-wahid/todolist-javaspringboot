package com.example.todolist.controller;

import com.example.todolist.entity.CheckList;
import com.example.todolist.entity.Item;
import com.example.todolist.model.response.CommonResponse;
import com.example.todolist.service.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/item")
public class ItemController {
    private final ItemService itemService;

    @GetMapping
    public ResponseEntity<?> getAll(){
        List<Item> itemList = itemService.getAll();
        return ResponseEntity.status(HttpStatus.OK.value())
                .body(CommonResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully get all Item")
                        .data(itemList)
                        .build());
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Item item){
        Item resultItem = itemService.create(item);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(CommonResponse.builder()
                        .statusCode(HttpStatus.CREATED.value())
                        .message("Successfully create new Item")
                        .data(resultItem)
                        .build());
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Item item) {
        Item resultItem = itemService.update(item);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CommonResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully update item")
                        .data(resultItem)
                        .build());
    }

    @PutMapping(path = "/status")
    public ResponseEntity<?> updateStatus(@RequestBody String id, Boolean status) {
        Item resultItem = itemService.updateStatus(id, status);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CommonResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully update status item")
                        .data(resultItem)
                        .build());
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteById(@PathVariable(name = "id") String id) {
        itemService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CommonResponse.<String>builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully delete item")
                        .build());
    }
}
