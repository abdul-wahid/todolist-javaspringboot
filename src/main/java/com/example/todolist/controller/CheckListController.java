package com.example.todolist.controller;

import com.example.todolist.entity.CheckList;
import com.example.todolist.model.response.CommonResponse;
import com.example.todolist.service.CheckListService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/check-list")
public class CheckListController {
    private final CheckListService checkListService;

    @GetMapping
    public ResponseEntity<?> getAll(){
        List<CheckList> checkLists = checkListService.getAll();
        return ResponseEntity.status(HttpStatus.OK.value())
                .body(CommonResponse.builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully get all CheckList")
                        .data(checkLists)
                        .build());
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody CheckList checkList){
        CheckList resultCheckList = checkListService.create(checkList);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(CommonResponse.builder()
                        .statusCode(HttpStatus.CREATED.value())
                        .message("Successfully create new CheckList")
                        .data(resultCheckList)
                        .build());
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteById(@PathVariable(name = "id") String id) {
        checkListService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(CommonResponse.<String>builder()
                        .statusCode(HttpStatus.OK.value())
                        .message("Successfully delete CheckList By Id")
                        .build());
    }
}
